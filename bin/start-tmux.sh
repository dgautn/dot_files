#!/bin/bash
#
# Setup a work space called `work` with two windows
# first window has 3 panes. 
# The first pane set at 65%, split horizontally, set to api root and running vim
# pane 2 is split at 25% and running redis-server 
# pane 3 is set to api root and bash prompt.
# note: `api` aliased to `cd ~/path/to/work`
#
session="start"

# set up tmux
tmux start-server

# create a new tmux session, starting vim from a saved session in the new window
#tmux new-session -d -s $session -n vim #"vim -S ~/.vim/sessions/kittybusiness"
tmux new-session -d -s $session # -n inicio

# Select pane 1, start ncspot 
tmux selectp -t 1 
tmux send-keys "ncspot" C-m 

# Split pane 1 vertical by 60%, start pyradio
tmux splitw -v -p 40
tmux send-keys "pyradio" C-m 

# Select pane 2 
#tmux selectp -t 2
# Split pane 2 horizontal
tmux splitw -h -p 3
tmux send-keys "clima_loop.sh" C-m 
#tmux send-keys 'watch -n 600 curl -H "Accept-Language: es" wttr.in/Cordoba_Capital_Argentina?Q0' C-m

# select pane 3, 
#tmux selectp -t 3
# Split pane 3 vertical
tmux splitw -v -p 98
tmux clock -t 4 

# Select pane 1
tmux selectp -t 1

# create a new window called 
tmux new-window -t $session:2 # -n BASH

# return to main window
tmux select-window -t $session:1

# Finished setup, attach to the tmux session!
tmux attach-session -t $session
