" ORIGINAL TEMPLATE -------------------------------------------------------" {{{

" All system-wide defaults are set in $VIMRUNTIME/debian.vim and sourced by
" the call to :runtime you can find below.  If you wish to change any of those
" settings, you should do it in this file (/etc/vim/vimrc), since debian.vim
" will be overwritten everytime an upgrade of the vim packages is performed.
" It is recommended to make changes after sourcing debian.vim since it alters
" the value of the 'compatible' option.

runtime! debian.vim

" Vim will load $VIMRUNTIME/defaults.vim if the user does not have a vimrc.
" This happens after /etc/vim/vimrc(.local) are loaded, so it will override
" any settings in these files.
" If you don't want that to happen, uncomment the below line to prevent
" defaults.vim from being loaded.
" let g:skip_defaults_vim = 1

" Uncomment the next line to make Vim more Vi-compatible
" NOTE: debian.vim sets 'nocompatible'.  Setting 'compatible' changes numerous
" options, so any other options should be set AFTER setting 'compatible'.
"set compatible

" Vim5 and later versions support syntax highlighting. Uncommenting the next
" line enables syntax highlighting by default.
if has("syntax")
  syntax on
endif

" If using a dark background within the editing area and syntax highlighting
" turn on this option as well
"set background=dark

" Uncomment the following to have Vim jump to the last position when
" reopening a file
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

" Uncomment the following to have Vim load indentation rules and plugins
" according to the detected filetype.
filetype plugin indent on

" The following are commented out as they cause vim to behave a lot
" differently from regular Vi. They are highly recommended though.
set showcmd     " Show (partial) command in status line.
set showmatch       " Show matching brackets.
"set ignorecase     " Do case insensitive matching
"set smartcase      " Do smart case matching
set incsearch       " Incremental search
"set autowrite      " Automatically save before commands like :next and :make
"set hidden     " Hide buffers when they are abandoned
set mouse=a     " Enable mouse usage (all modes)

" Source a global configuration file if available
if filereadable("/etc/vim/vimrc.local")
  source /etc/vim/vimrc.local
endif

" }}}

" -----------------------------------------------------------------------------
"               CONFIG PERSONAL
" -----------------------------------------------------------------------------

" FUNCIONES ---------------------------------------------------------------- {{{
"
"function! ISort()
"    "!! python3 -c 'import isort; isort.code("import b\nimport a\n")'
"    let line_start = line("'<")
"    let line_end= line("'>")
"    "let [line_start, column_start] = getpos("'<")[1:2]
"    "let [line_end, column_end] = getpos("'>")[1:2]
"    let lines = getline(line_start, line_end)
"    if len(lines) == 0
"        "return ''
"        return
"    endif
"    let listImport = join(lines, "\\n")
"    "let lines[-1] = lines[-1][: column_end - (&selection == 'inclusive' ? 1 : 2)]
"    "let lines[0] = lines[0][column_start - 1:]
"    "let pkgImport = join(lines, "\\n")
"    ""echo join(lines, "-")
"    "let strArgs = 'python3 -c '."\'".'import isort; print(isort.code(\"'.pkgImport.'\"))'."\'"
"    ""echo strArgs
"    ""! python3 -c 'import isort; isort.code("' . pkgImport . '")'
"    "let orderedImport = system(strArgs)
"    "exe "normal a".orderedImport
"python3 << EOF
"import vim
"import isort
"#s = "I was set in python"
"#vim.command("let sInVim = '%s'"% s)
"import_list = vim.eval("listImport")
"import_orden = isort.code(import_list)
"#vim.command("let importOrdenVim = {}".format(import_orden))
"print(len(import_list))
"print(len(import_orden))
"EOF
"endfunction

function! TabsYNums(space)
    let &l:tabstop = a:space
    let &l:shiftwidth = a:space
    "set tabstop=4
    "set shiftwidth=4
    setlocal expandtab
    setlocal nu
endfunction

function! ColumnMax(max)
    highlight ColorColumn ctermbg=234
    highlight ColorColumn ctermfg=244
    let &l:colorcolumn = a:max
endfunction

function! Diff_Original()
    vert new
    set buftype=nofile
    " lee y pega el contenido del buffer alternativo
    read #
    0d_
    diffthis
    wincmd p
    diffthis
endfunction
command DiffOrig call Diff_Original()

function! ToggleStatusLine()
    let l:aux = &laststatus
    if !exists("l:prev_state")
        let l:prev_state = &laststatus
        if &laststatus == 2
            let &laststatus = 1
        else
            let &laststatus = 2
        endif
    else
        let &laststatus = l:prev_state
        let l:prev_state = l:aux
    endif
endfunction
command ToggleSL call ToggleStatusLine()

function! CheckString()
    syn region singleQuote start=+'+ end=+'+ contains=@Spell
    syn region doubleQuote start=+"+ end=+"+ contains=@Spell
endfunction

function! GitBranch()
  return system("git rev-parse --abbrev-ref HEAD 2>/dev/null | tr -d '\n'")
endfunction

function! StatuslineGit()
  let l:branchname = GitBranch()
  return strlen(l:branchname) > 0?'  '.l:branchname.' ':''
endfunction

function! IfOtherPath()
    let l:fileTail = expand("%:t")
    let l:fileFull = expand("%:f")
    let l:fileName = l:fileTail == l:fileFull ? '' : l:fileFull  
    if (strlen(l:fileName) > 35)
        let l:newName = ''
        for l:fldr in split(l:fileName, '/', 1)[:-2]
            let l:newName = l:newName .. fldr[:0] .. '/'
        endfor
        let l:newName = l:newName .. split(l:fileName, '/', 1)[-1]
        let l:fileName = l:newName
    endif
    if (strlen(l:fileName) > 35)
        let l:fileName = '...' .. l:fileName[-31:]
    endif
    "return expand("%:f") == expand("%:t") ?'':expand("%:f")
    return l:fileName
endfunction 

function! ShowTab()
    let l:tab_level = ( col( '.' ) - 1 ) / &ts
    if ( ( l:tab_level * &ts + 1 ) != col( '.' ) )
         let l:tab_level = '*'
    endif
    return l:tab_level
endfunction

function! MinimalStatusLine()
    let l:status = "\ %y%=%4p%%\ "
    return l:status
endfunction

function! TwiddleCase(str)
  if a:str ==# toupper(a:str)
    let result = tolower(a:str)
  elseif a:str ==# tolower(a:str)
    let result = substitute(a:str,'\(\<\w\+\>\)', '\u\1', 'g')
  else
    let result = toupper(a:str)
  endif
  return result
endfunction
vnoremap ~ y:call setreg('', TwiddleCase(@"), getregtype(''))<CR>gv""Pgv

" J-Split   https://github.com/mgamba/j-split.git
" Split current line into multiple lines
"
" Split by contiguous spaces into multiple lines and re-indent new lines
"function! JSplit()
"  let s:startline = line(".")

"  s/\([^^ ]\) \+/\1\r/g

"  call ReindentTo(s:startline)
"endfunction

"function! ReindentTo(line_number)
"  normal Vt
"  exec a:line_number
"  normal =
"endfunction
" }}}

" Automatic reloading of .vimrc (no funciona)
"autocmd! bufwritepost .vimrc source %
"autocmd! BufWritePost $MYVIMRC source % | echom "Reloaded $MYVIMRC"

" Para usar como source el archivo .vimrc si está presente en el directorio de trabajo
" set exrc
" set secure

" Tabulaciones y numeros
set tabstop=2
set shiftwidth=2
set expandtab
set nu
"call TabsYNums(2)

" Muestra tabulaciones y espacios al final de linea
set list
set listchars=trail:·
set listchars=tab:··

" UbicaciOn de nuevas ventanas (split)
set splitbelow
set splitright

" Deshabilitar limite de columnas 
set textwidth=0

" Divide lineas solo visualmente
set wrap
set linebreak

" Statusbar visible
set laststatus=2

" Use highlighting when doing a search.
set hlsearch

" ------------------
" show matching bracket for 0.2 seconds
set matchtime=3
" ------------------
  
" ------------------
" Do not save backup files.
"set nobackup

" Do not let cursor scroll below or above N number of lines when scrolling.
set scrolloff=2

" Do not wrap lines. Allow long lines to extend as far as the line goes.
"set nowrap

" Set the commands to save in history default number is 20.
set history=1000
" ------------------

" ------------------
" Enable auto completion menu after pressing TAB.
set wildmenu

" Make wildmenu behave like similar to Bash completion.
"set wildmode=list:longest

" There are certain files that we would never want to edit with Vim.
" Wildmenu will ignore files with these extensions.
"set wildignore=*.docx,*.jpg,*.png,*.gif,*.pdf,*.pyc,*.exe,*.flv,*.img,*.xlsx
set wildignore+=*/tmp/*,*.so,*.swp,*.zip
" ------------------

" check spell
set spelllang=es
"set nospell

" utiliza el programa externo 'par' para formatear texto
set formatprg=par

" COLORS ------------------------------------------------------------------- {{{

colorscheme pablo
" colorscheme dracula

" espacios finales y tabs
hi SpecialKey ctermfg=red

" colores para menus desplegables de autocompletado (uso en jedi)
highlight Pmenu ctermbg=DarkGrey
highlight PmenuSel ctermbg=Grey ctermfg=DarkRed
highlight jediFunction ctermbg=238
highlight jediFat cterm=underline,bold ctermfg=1  ctermbg=248
"highlight PmenuSel 

" colores para barra de comandos
"hi User1 ctermbg=234  ctermfg=146
hi User1              ctermfg=146
hi User2 ctermbg=234  ctermfg=153
hi User3 ctermbg=241  ctermfg=17
hi User4 ctermbg=249  ctermfg=18
" statusline
hi StatusLine   ctermbg=8    ctermfg=14
hi StatusLineNC ctermbg=7    ctermfg=236

hi User5 ctermbg=17  ctermfg=250
hi User6 ctermbg=19  ctermfg=239
hi User7 ctermbg=27  ctermfg=233

hi User8 cterm=bold  ctermbg=214 ctermfg=234
hi User9 cterm=bold  ctermbg=9   ctermfg=234

" color para mostrar el modo en la última linea e.g. '-- INSERT --'
hi ModeMsg cterm=bold ctermbg=17  ctermfg=39

" color column for max line length
highlight ColorColumn ctermbg=234
highlight ColorColumn ctermfg=244

"" para que se vea mejor en vim (mejorar)
"hi SpellBad cterm=bold,underline ctermbg=black
"hi SpellCap cterm=underline ctermbg=black

" }}}

" BARRA DE COMANDOS y STATUSLINE ------------------------------------------- {{{
"
" RULER ----------------------
"set rulerformat=%64(%1*%32.32(%<%t\ %8*%m%2*%y%)%3*%10.10(%P\ %)%4*%22.22(\ %l:%c%V\ %LL%4p%%\ %)%)%*
set rulerformat=%64(%1*%35.35(%<%t\ %8*%m%2*%y%)%3*%10.10(%P\ %)%4*%19.19(\ %l:%c%V\ %4p%%\ %)%)%*
" ----------------------------
  
" STATUSLINE -----------------
set statusline=
set statusline+=%(\ \|%t\|\ %)
set statusline+=%3.(%8*%m%)
set statusline+=%*
set statusline+=\ %y
set statusline+=\ %{FugitiveStatusline()}
"set statusline+=\ %{gitbranch#name()}
"set statusline+=%{StatuslineGit()}
set statusline+=\ %{IfOtherPath()}
set statusline+=\ %9*
set statusline+=%{CapsLockStatusline()}
set statusline+=%*
"   ----------
set statusline+=%=
"   ----------
"set statusline+=%#warningmsg#
set statusline+=%9*
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
"   ----------
set statusline+=%(\|\ %L\ lineas\ \|%)
set statusline+=%10.(\ %P\ %)
set statusline+=%31.(·\ l:%l,c:%c%V\ t:%{ShowTab()}\ ·\ %4p%%\ %)
" ----------------------------
  
" }}}

" CONFIG PLUGINS ----------------------------------------------------------- {{{

" ----------------
" editorconfig-vim
" ----------------
let g:EditorConfig_exclude_patterns = ['fugitive://.*']

" -------------
" python-syntax
" -------------
let g:python_highlight_all = 1
let g:python_highlight_func_calls = 0

" -----------
" voom
" -----------
let g:voom_latex_elements = '^\s*\\(begin\s*\{(document|abstract|thebibliography|question|solution|enumerate|item)\}|end\s*\{document\}|bibliography\s*\{)'

" -----------
" python-mode
" -----------
"let g:pymode_rope = 0
"let g:pymode_lint = 0

" ---------
" jedi-vim
" ---------
"let g:jedi#auto_initialization = 0
let g:jedi#popup_select_first = 0
let g:jedi#use_splits_not_buffers = "left"
"let g:jedi#show_call_signatures = 2

" -------------
" NERDCommenter
" -------------
let g:NERDDefaultAlign = 'left'
let g:NERDSpaceDelims = 1
let g:NERDRemoveExtraSpaces = 1
let g:NERDTrimTrailingWhitespace = 1
let g:NERDCustomDelimiters = {
    \ 'arduino': { 'left': '//', 'leftAlt': '/*', 'rightAlt': '*/' },
    \ 'Dockerfile': { 'left': '#' },
    \ }

" -------
" rainbow
" -------
let g:rainbow_active = 1 "set to 0 if you want to enable it later via :RainbowToggle

" --------------
" clang_complete
" --------------
let g:clang_library_path='/usr/lib/llvm-10/lib/libclang.so.1'
let g:clang_close_preview = 1
let g:clang_complete_macros = 1
" let g:clang_jumpto_declaration_key = '<C-}>'
" let g:clang_jumpto_back_key = '<C-T>'

" ----------
" Auto-Pairs
" ----------
" Para compatibilidad con clang_complete
let g:AutoPairsMapCR = 0
imap <silent><CR> <CR><Plug>AutoPairsReturn

" ------------
" vim-markdown
" ------------
let g:vim_markdown_conceal = 0
let g:tex_conceal = ""
let g:vim_markdown_math = 1
let g:vim_markdown_conceal_code_blocks = 0

" ----------------
" markdown-preview
" ----------------
" set to 1, nvim will open the preview window after entering the markdown buffer
" default: 0
let g:mkdp_auto_start = 0

" set to 1, the nvim will auto close current preview window when change
" from markdown buffer to another buffer
" default: 1
let g:mkdp_auto_close = 1

" set to 1, the vim will refresh markdown when save the buffer or
" leave from insert mode, default 0 is auto refresh markdown as you edit or
" move the cursor
" default: 0
let g:mkdp_refresh_slow = 0

" ------------------------------
" ConfiguraciOn para Pydocstring
" ------------------------------
let g:pydocstring_doq_path='/home/gustavo/.local/bin/doq'
"let g:pydocstring_enable_mapping=0
let g:pydocstring_formatter='sphinx'

" ---------
" Colorizer (pone lento todo vim)
" ---------
"let g:colorizer_auto_filetype='css,html,htm,vim,tmux'
""let g:colorizer_auto_filetype='vim'
"let g:colorizer_skip_comments = 1
"let g:colorizer_disable_bufleave = 1
"let g:colorizer_textchangedi = 0ou could also benefit from the great help syst

" ---------
" incsearch
" ---------
" Use highlighting when doing a search.
"set hlsearch
let g:incsearch#auto_nohlsearch = 1
"map n  <Plug>(incsearch-nohl-n)
"map N  <Plug>(incsearch-nohl-N)
"map *  <Plug>(incsearch-nohl-*)
"map #  <Plug>(incsearch-nohl-#)
"map g* <Plug>(incsearch-nohl-g*)
"map g# <Plug>(incsearch-nohl-g#)

" --------
" vim-mark
" --------
"let g:mw_no_mappings = 1

" --------------
" vim-translator 
" --------------
let g:translator_target_lang='es'
let g:translator_default_engines=['google', 'bing']

" ---------
" syntastic
" ---------
"set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
"set statusline+=%*

"let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 1
"let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_python_python_exec = '/usr/bin/python3'
let g:syntastic_python_checkers = ['flake8', 'pycodestyle']
let g:syntastic_always_populate_loc_list = 1

" --------
" NERDTree
" --------
let g:NERDTreeCustomOpenArgs = {'file': {'keepopen': 1, 'reuse': 'all', 'where': 'p'}, 'dir': {}}

" ----------
" indentLine
" ----------
"set conceallevel=0
"set concealcursor=n
"let g:indentLine_concealcursor = &concealcursor
"let g:indentLine_conceallevel = &conceallevel

" ------
" VimTex
" ------
"let g:vimtex_syntax_conceal_disable = 1
let g:vimtex_syntax_conceal = {
      \ 'accents': 0,
      \ 'cites': 0,
      \ 'fancy': 0,
      \ 'greek': 0,
      \ 'math_bounds': 0,
      \ 'math_delimiters': 0,
      \ 'math_fracs': 0,
      \ 'math_super_sub': 0,
      \ 'math_symbols': 0,
      \ 'sections': 0,
      \ 'styles': 0,
      \}
let g:tex_comment_nospell=1
let g:vimtex_include_search_enabled=0
let g:vimtex_quickfix_autoclose_after_keystrokes=1

let g:vimtex_compiler_latexmk_engines = {
    \ '_'                : '-pdfdvi',
    \ 'pdfdvi'           : '-pdfdvi',
    \ 'pdfps'            : '-pdfps',
    \ 'pdflatex'         : '-pdf',
    \ 'luatex'           : '-lualatex',
    \ 'lualatex'         : '-lualatex',
    \ 'xelatex'          : '-xelatex',
    \ 'context (pdftex)' : '-pdf -pdflatex=texexec',
    \ 'context (luatex)' : '-pdf -pdflatex=context',
    \ 'context (xetex)'  : '-pdf -pdflatex=''texexec --xtx''',
    \}

" -----
" CtrlP 
" -----
let g:ctrlp_custom_ignore = {
    \ 'dir':  '\v[\/]\.git$',
    \ 'file': '\v\.(swp|so|pdf|zip|png|jpg|jpeg|pgm)$',
    \ }

" -------
" Vim-Ros
" -------
let g:ros_make='catkin build'
let g:ros_disable_warnings=0

" --------
" Vim-Bujo
" --------
let g:bujo#window_width = 70

" ------
" Bracey
" ------
let g:bracey_refresh_on_save = 1

" ------------------------
" vim-colorscheme-switcher
" ------------------------
let g:colorscheme_switcher_define_mappings = 0

" ---------
" UltiSnips
" ---------
let g:UltiSnipsExpandTrigger = "<s-tab><s-tab>"
"let g:UltiSnipsJumpForwardTrigger = '<s-tab>'
"let g:UltiSnipsJumpBackwardTrigger = '<s-bs>'

let g:UltiSnipsEditSplit = "vertical"
let g:UltiSnipsListSnippets = "<s-tab>"
" }}}

" MAPPINGS ----------------------------------------------------------------- {{{

" guardar como sudo
cmap w!! w !sudo tee > /dev/null %

" mapea la tecla <leader> a ','
let mapleader = ","

" localleader, para vimtex principalmente
let maplocalleader = ","

" check spell
nnoremap <F8> :setlocal spell! spell?<CR>
inoremap <F8> <C-o>:setlocal spell! spell?<CR>

" Busqueda con barra espaciadora
nmap <Space> /

" Redibujar pantalla
nnoremap <F5> :redraw!<CR>
inoremap <F5> <ESC>:redraw!<CR>a

" Movimiento entre ventanas
nmap <silent> <C-k> :wincmd k<CR>
nmap <silent> <C-j> :wincmd j<CR>
nmap <silent> <C-h> :wincmd h<CR>
nmap <silent> <C-l> :wincmd l<CR>

" Quickly insert an empty new line without entering insert mode
"nnoremap <Leader>o o<Esc>0"_D
"nnoremap <Leader>O O<Esc>0"_D
nmap <Leader>o o<Esc>
nmap <Leader>O O<Esc>

"nmap <Leader><Space> :ToggleSL<CR>
nmap <Leader>= :ToggleSL<CR>

" formatear ancho de linea
"nnoremap <Leader>j :call JSplit()<cr>
map <Leader>j {v}!par -w78<CR>
vmap <Leader>j !par -w78<CR>
"map <Leader>j {v}!par -jw78<CR>
"vmap <Leader>j !par -jw78<CR>

" --------------
" undotree
" --------------
nnoremap <leader>u :UndotreeToggle<CR>

" --------------
" NERDTree
" --------------
nnoremap <leader><Bs> :NERDTreeToggle<CR>

" --------------
" easy-align
" --------------
nmap ga <Plug>(EasyAlign)
xmap ga <Plug>(EasyAlign)

" --------------
" vim-translator 
" --------------
" Echo translation in the cmdline
nmap <silent> <Leader>tt <Plug>Translate
vmap <silent> <Leader>tt <Plug>TranslateV
" Display translation in a window
nmap <silent> <Leader>tw <Plug>TranslateW
vmap <silent> <Leader>tw <Plug>TranslateWV
" Replace the text with translation
nmap <silent> <Leader>tr <Plug>TranslateR
vmap <silent> <Leader>tr <Plug>TranslateRV
" Translate the text in clipboard
nmap <silent> <Leader>tx <Plug>TranslateX

" ---------------
" vim-pydocstring
" ---------------
nmap <silent> <Leader>l <Plug>(pydocstring)
vmap <silent> <Leader>l :Pydocstring<CR>

" --------
" vim-mark
" --------
nmap <Plug>IgnoreMarkSearchNext <Plug>MarkSearchNext
nmap <Plug>IgnoreMarkSearchPrev <Plug>MarkSearchPrev
"xmap <Leader>m <Plug>MarkSet
"nmap <Leader>m <Plug>MarkSet
nmap <Leader>M <Plug>MarkToggle
nmap <Leader>N <Plug>MarkAllClear

" ---------
" incsearch
" ---------
map /  <Plug>(incsearch-forward)
map ?  <Plug>(incsearch-backward)
map g/ <Plug>(incsearch-stay)
map n  <Plug>(incsearch-nohl-n)
map N  <Plug>(incsearch-nohl-N)
map *  <Plug>(incsearch-nohl-*)
map #  <Plug>(incsearch-nohl-#)
map g* <Plug>(incsearch-nohl-g*)
map g# <Plug>(incsearch-nohl-g#)

" ----------
" uninpaired
" ----------
nmap { [
nmap } ]
omap { [
omap } ]
xmap { [
xmap } ]

" ------------------------
" vim-colorscheme-switcher
" ------------------------
inoremap <silent> <F7> <C-O>:NextColorScheme<CR>
nnoremap <silent> <F7> :NextColorScheme<CR>
inoremap <silent> <F6> <C-O>:PrevColorScheme<CR>
nnoremap <silent> <F6> :PrevColorScheme<CR>
nnoremap <silent> <F6><F6> :colorscheme pablo<CR>

" --------
" Vim-Bujo
" --------
" nmap <C-N> <Plug>BujoAddnormal
" imap <C-N> <Plug>BujoAddinsert
" nmap <C-C> <Plug>BujoChecknormal
" imap <C-C> <Plug>BujoCheckinsert
" }}}

" VIMSCRIPT ---------------------------------------------------------------- {{{

"augroup vimrc_incsearch_highlight
    "autocmd!
    "autocmd CmdlineEnter /,\? :set hlsearch
    "autocmd CmdlineLeave /,\? :set nohlsearch
"augroup END

augroup supertab
    autocmd!
    autocmd FileType *
                \ if &omnifunc != '' |
                \   call SuperTabChain(&omnifunc, "<c-p>") |
                \ endif
    "autocmd OptionSet spell
    "            \ if !(v:option_new) && (&omnifunc != '') |
    "            \   call SuperTabChain(&omnifunc, "<c-p>") |
    "            \ endif
augroup END

augroup tmux_rename_window
    autocmd!
    if exists('$TMUX')
        let windowID = system("tmux display-message -p '#{window_id}'")
        autocmd BufReadPost,FileReadPost,BufNewFile,FocusGained,WinEnter *
                \ call system("tmux rename-window '[" . expand("%:t") . "]'")
        "autocmd VimLeave,FocusLost * call system("tmux set-window-option automatic-rename")
        autocmd VimLeave * call system("tmux set-window-option automatic-rename")
        " Para prevenir que renombre otra ventana
        autocmd FocusLost * 
                \ if system("tmux display-message -p '#{window_id}'") == windowID 
                \ | call system("tmux set-window-option automatic-rename")
                \ | endif
    endif
augroup END

" CONFIG FILETYPES
" ----------------

" HELP -------------------------------------------
augroup filetype_help
    autocmd!
    autocmd FileType help setlocal statusline=%(\ \|%t\|\ %y%)%=%(%4p%%\ %)
augroup END   
" ------------------------------------------------

" VOOM -------------------------------------------
augroup filetype_voom
    autocmd!
    autocmd FileType voomtree setlocal statusline=%!MinimalStatusLine()
    autocmd FileType voomtree setlocal nonumber
augroup END   
" ------------------------------------------------

" NERD TREE --------------------------------------
augroup filetype_nerdtree
    autocmd!
    autocmd FileType nerdtree setlocal statusline=%!MinimalStatusLine()
    autocmd FileType nerdtree setlocal nonumber
augroup END   
" ------------------------------------------------

" MARKDOWN ---------------------------------------
augroup markdown
    autocmd!
    autocmd FileType markdown setlocal conceallevel=0 |
                \ let g:indentLine_conceallevel = &conceallevel
"set conceallevel=0
"set concealcursor=n
"let g:indentLine_concealcursor = &concealcursor
"let g:indentLine_conceallevel = &conceallevel
augroup END   
" ------------------------------------------------

    "setlocal nonumber
" HTML -------------------------------------------
augroup filetype_html
    autocmd!
    autocmd FileType html call TabsYNums(2)
    autocmd FileType html nmap <Leader><Space> :Voom html<CR>
    autocmd FileType html setlocal foldmethod=syntax
    autocmd FileType html setlocal foldlevel=22
    "autocmd FileType html normal zR
augroup END   
" ------------------------------------------------

" VIM --------------------------------------------
" This will enable code folding.
" Use the marker method of folding.
augroup filetype_vim
    autocmd!
    autocmd FileType vim setlocal foldmethod=marker
    autocmd FileType vim call TabsYNums(4)
    autocmd FileType vim call ColumnMax(80)
    "autocmd FileType vim exe ColorHighlight!
augroup END
" ------------------------------------------------

" CONFIG -----------------------------------------
augroup filetype_conf
    autocmd!
    autocmd FileType conf setlocal foldmethod=marker
augroup END   
" ------------------------------------------------

" TEX --------------------------------------------
augroup filetype_tex
    autocmd!
    autocmd FileType tex call TabsYNums(2)
    autocmd FileType tex nmap <Leader><Space> :Voom latex<CR>
    autocmd FileType tex let g:surround_{char2nr('c')} = "\\\1command\1{\r}"
    "autocmd FileType tex let g:SuperTabDefaultCompletionType="<c-x><c-o>"
    "autocmd FileType tex
    "    \ if &omnifunc != '' |
    "    \   call SuperTabChain(&omnifunc, "<c-p>") |
    "    \ endif
augroup END   
" ------------------------------------------------

" TMUX -------------------------------------------
augroup filetype_tmux
    autocmd!
    autocmd FileType tmux setlocal foldmethod=marker
    "autocmd FileType tmux exe ColorSwapFgBg
augroup END   
" ------------------------------------------------

" PYTHON -----------------------------------------
augroup filetype_python
    autocmd!
    " no mostrar docstrings en nueva ventana en jedi
    autocmd FileType python setlocal completeopt-=preview
    " Para cambiar tab por espacios en Python
    autocmd FileType python call ColumnMax(80) | call TabsYNums(4)
    " Marcadores de pliegues
    "autocmd FileType python PyDocHide
    autocmd FileType python nmap <Leader><Space> :Voom python<CR>
    autocmd FileType python setlocal foldmethod=marker
    autocmd FileType python
                \ if pydocstring_formatter == 'sphinx' |
                \   let g:pydocstring_templates_path='/home/gustavo/.vim/pydocstring-templates' |
                \ endif
    "autocmd FileType python setlocal foldmethod=indent
    "autocmd FileType python setlocal foldopen=all
    "autocmd FileType python setlocal foldlevel=99
    " para que se vea mejor en vim (mejorar)
    "autocmd FileType python hi SpellBad cterm=bold,underline ctermbg=black
    "autocmd FileType python hi SpellCap cterm=underline ctermbg=black
    " Para introducir encabezado
    autocmd BufNewFile *.py 0r /home/gustavo/.vim/headers/python
    autocmd BufNewFile *.py exe "%s/YYYY/" .strftime("%Y")
    autocmd BufNewFile *.py exe "%s/SSSS/" .expand("%") | execute "normal G"
augroup END
" ------------------------------------------------

" OPENSCAD ---------------------------------------
augroup filetype_openscad
    autocmd!
    autocmd BufRead,BufNewFile *.scad setlocal autoindent | setlocal smartindent
    autocmd BufRead,BufNewFile *.scad call TabsYNums(2)
    autocmd BufRead,BufNewFile *.scad setlocal omnifunc=syntaxcomplete#Complete
    autocmd BufReadPre *.scad execute 'silent !openscad 2>/dev/null % &' | sleep "0.2" | redraw!
    " Para introducir encabezado
    autocmd BufNewFile *.scad 0r /home/gustavo/.vim/headers/openscad
    autocmd BufNewFile *.scad exe "%s/YYYY/" .strftime("%Y") | execute "normal G"
    "autocmd BufRead,BufNewFile *.scad
    "    \ if &omnifunc != '' |
    "    \   call SuperTabChain(&omnifunc, "<c-p>") |
    "    \ endif
augroup END
" ------------------------------------------------

" ARDUINO ----------------------------------------
augroup filetype_arduino
    autocmd!
    autocmd BufRead,BufNewFile *.ino setlocal autoindent | setlocal smartindent
    autocmd BufRead,BufNewFile *.ino call TabsYNums(2)
    autocmd BufRead,BufNewFile *.ino setlocal omnifunc=syntaxcomplete#Complete
    autocmd BufReadPre *.ino execute 'silent !arduino 2>/dev/null % &' | sleep "2.2" | redraw!
augroup END
" ------------------------------------------------

" LANZADORES -------------------------------------
augroup filetye_desktop
    autocmd!
    autocmd BufNewFile *.desktop 0r /home/gustavo/.vim/headers/desktop
    autocmd BufNewFile *.desktop exe "%s/<<NAME>>/" .expand("%") | exe "%s/.desktop/"
augroup END
" ------------------------------------------------

" SH SHELL SCRIPT ------------------------------
augroup filetype_sh
    autocmd!
    " Para cambiar tab por espacio
    autocmd FileType sh call TabsYNums(4)
    " Para introducir encabezado
    autocmd BufNewFile *.sh 0r /home/gustavo/.vim/headers/sh
    autocmd BufNewFile *.sh exe "%s/YYYY/" .strftime("%Y") | execute "normal G"
augroup END
" ------------------------------------------------

" BASH SHELL SCRIPT ------------------------------
augroup filetype_bash
    autocmd!
    " Para cambiar tab por espacio
    autocmd FileType bash call TabsYNums(4)
    " Para introducir encabezado
    autocmd BufNewFile *.bash 0r /home/gustavo/.vim/headers/bash
    autocmd BufNewFile *.bash exe "%s/YYYY/" .strftime("%Y") | execute "normal G"
augroup END
" ------------------------------------------------

" ROSLAUNCH --------------------------------------
augroup filetype_roslaunch
    autocmd!
    autocmd BufRead,BufNewFile *.launch setfiletype roslaunch
augroup END
" ------------------------------------------------

" }}}
