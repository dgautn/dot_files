#!/usr/bin/env sh
# code adapted from:
# https://gist.github.com/hannesfrank/35f4472c8294ddeb26055672d2114bd9

# for windows change shebang possibly? 
# and figure out how to install pdftk?
# apologies, I dont use windows :(

if [[ $# -eq 0 ]] ; then
    echo 'usage:' ${0##*/} '<input pdf> <output pdf>'
    exit 0
fi

IN_PDF=$1
OUT_PDF=$2

pdftk $IN_PDF cat $(\
  pdftk $IN_PDF dump_data \
        | grep PageLabelNewIndex `# filter page labels`\
        | cut -d' ' -f2   `# get label number`\
        | tail -n +2         `# skip first line`\
        | awk '{print ($1 - 1)}' `# subtract one to get previous page`\
  ) end \
  output $OUT_PDF