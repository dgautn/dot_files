#!/bin/bash
#
# # (c) 2021, Gustavo AlbarrAn

# idea original: (c) 2013, Abbafei
# https://stackoverflow.com/a/18692600

Directorios=("." "./bin" "./tmux_layouts" "./.vim/headers" "./.vim/pydocstring-templates" "./.config/ncspot" "./.config/pyradio")

find_links () {
    #buscar links pero descartar directorios
    for LINK in $(find $1 -maxdepth 1 -type l -exec test '!' -d {} ';' -print)
    do 
        echo siguiendo $LINK 
        git rm --cached $LINK
        diff -au /dev/null $LINK | git apply --cached -p1 -
    done
    }

for directorio in ${Directorios[*]}
do
    echo en $directorio :
    find_links $directorio
done
