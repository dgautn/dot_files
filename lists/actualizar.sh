#!/bin/bash
#
# (c) 2021, Gustavo AlbarrAn

echo -e "# Lista de paquetes instalados \n" > pkg_instalados.txt
dpkg --get-selections | awk '$2 ~ /^install$/ {print $1}' >> pkg_instalados.txt

echo -e "# Paquetes instalados manualmente \n" > pkg_instalados_manual.txt
list-manually-installed-packages.sh >> pkg_instalados_manual.txt

echo -e "# Paquetes pip instalados \n" > pip_instalados.txt
pip list >> pip_instalados.txt

echo -e "# Paquetes pip instalados manualmente \n" > pip_instalados_manual.txt
pip list --user >> pip_instalados_manual.txt

echo -e "# Plugins de vim \n" > vim_plugins.txt
ls -1 ~/.vim/pack/plugins/start/ >> vim_plugins.txt
