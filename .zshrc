# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="/home/gustavo/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
#ZSH_THEME="robbyrussell"
#ZSH_THEME="jovial"
ZSH_THEME="powerlevel10k/powerlevel10k"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# Caution: this setting can cause issues with multiline prompts (zsh 5.7.1 and newer seem to work)
# See https://github.com/ohmyzsh/ohmyzsh/issues/5765
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
#
# The `zsh_reload` plugin is deprecated and will be removed.
# Use `omz reload` or `exec zsh` instead.
plugins=(
  git
  #autojump
  #urltools
  #bgnotify
  zsh-autosuggestions
  zsh-syntax-highlighting
  zsh-history-enquirer
  #jovial
  #osx
  #command-time
  command-not-found
  #colored-man-pages
  web-search
  k
  #unified-titles
  #tmux-zsh-vim-titles
  #zsh_reload
  calc
  python
  pip
  #vi-mode
  #thefuck
  git-flow
  #zsh-autocomplete
  #history-substring-search
  npm
  ng
  zsh-z
)

source $ZSH/oh-my-zsh.sh

# User configuration

############ Plugins configuration ###########
# zsh-autocomplete
zstyle ':autocomplete:*' min-input 1
# Up arrow:
# bindkey '\e[A' up-line-or-history
# bindkey '\eOA' up-line-or-history
# Down arrow:
# bindkey '\e[B' down-line-or-history
# bindkey '\eOB' down-line-or-history
# up-line-or-search:  Open history menu.
# up-line-or-history: Cycle to previous history line.
# Uncomment the following lines to disable live history search:
# zle -A {.,}history-incremental-search-forward
# zle -A {.,}history-incremental-search-backward

# history-substring-search
#bindkey "^[[A" history-substring-search-up
#bindkey "^[[B" history-substring-search-down
##############################################

# https://stackoverflow.com/a/13614816
autoload -Uz history-beginning-search-menu
zle -N history-beginning-search-menu
bindkey '^R^R' history-beginning-search-menu
bindkey '^R' history_enquire

# https://stackoverflow.com/a/58517668
autoload -Uz compinit && compinit

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
alias zshconf="$EDITOR $HOME/.zshrc"
alias vimconf="$EDITOR $HOME/.vimrc"
alias tmuxconf="$EDITOR $HOME/.tmux.conf"
alias i3conf="$EDITOR /home/gustavo/.config/i3/config"
alias kittyconf="$EDITOR $HOME/.config/kitty/kitty.conf"
alias alttyconf="$EDITOR $HOME/.config/alacritty/alacritty.yml"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# rngr() {
	#ranger ; tmux set-window-option automatic-rename on
# }
#alias rang="ranger ; tmux set-window-option automatic-rename on"
alias get_idf='. $HOME/esp32/esp-idf/export.sh'
alias k="k -h"
alias byebye="systemctl poweroff -i"

alias -s pdf=evince
alias -s txt=vim
alias -s {jpg,jpeg,gif,png}=eog
alias -s ipynb='jupyter notebook'
alias -s {odt,ods}=libreoffice

hash -d utn=/mnt/Documentos/Facultad

# thefuck
eval $(thefuck --alias)

# If command execution time above min. time, plugins will not output time.
ZSH_COMMAND_TIME_MIN_SECONDS=3

# Message to display (set to "" for disable).
ZSH_COMMAND_TIME_MSG="└───┤ %s ├───┘"

# Message color.
ZSH_COMMAND_TIME_COLOR="cyan"

# Exclude some commands
ZSH_COMMAND_TIME_EXCLUDE=(vim)

#export NVM_DIR="$HOME/.nvm"
#[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
#[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

#PATH="/home/gustavo/perl5/bin${PATH:+:${PATH}}"; export PATH;
#PERL5LIB="/home/gustavo/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
#PERL_LOCAL_LIB_ROOT="/home/gustavo/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
#PERL_MB_OPT="--install_base \"/home/gustavo/perl5\""; export PERL_MB_OPT;
#PERL_MM_OPT="INSTALL_BASE=/home/gustavo/perl5"; export PERL_MM_OPT;

. /usr/share/autojump/autojump.sh

# Para deshabilitar función de CTRL-S
# https://superuser.com/a/1390983
# tambien https://unix.stackexchange.com/a/515257/307359
#stty ixoff

# ROS
source /opt/ros/noetic/setup.zsh
#source $HOME/catkin_ws_imove/devel/setup.zsh
#source $HOME/catkin_ws_imove/ros_ip.sh
# source /home/gustavo/catkin_ws_dresina/devel/setup.zsh

# Kiri environment setup NOTE: mover a .profile
eval $(opam env)
export KIRI_HOME=${HOME}/.local/share/kiri
export PATH=${KIRI_HOME}/submodules/KiCad-Diff/:${PATH}
export PATH=${KIRI_HOME}/bin:${PATH}
