#!/bin/bash
#
# (c) 2021, Gustavo AlbarrAn

cd lists
./actualizar.sh
cd ..

git add .

./follow_links.sh

git commit -m "`date +'%d-%m-%Y_%s'`"
git push
